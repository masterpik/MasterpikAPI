package com.masterpik.api.events;

import com.masterpik.api.events.custom.messaging.Messaging;

public class Events {

    public static void init() {
        Messaging.init();
    }

}
