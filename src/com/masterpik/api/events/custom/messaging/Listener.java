package com.masterpik.api.events.custom.messaging;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.masterpik.api.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class Listener implements org.bukkit.event.Listener, PluginMessageListener {

    public static void init() {
        Bukkit.getPluginManager().registerEvents(new Listener(), Main.plugin);
        Bukkit.getMessenger().registerIncomingPluginChannel(Main.plugin, "BungeeCord", new Listener());
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {

        ByteArrayDataInput in = ByteStreams.newDataInput(message);

        String subchannel = in.readUTF();
        String getMessage = in.readUTF();

        ForwardMessageEvent event = new ForwardMessageEvent(channel, subchannel, getMessage);
        Bukkit.getPluginManager().callEvent(event);
    }
}
