package com.masterpik.api.events.custom.messaging;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ForwardMessageEvent extends Event {

    public static final HandlerList handlers = new HandlerList();

    protected String channel;
    protected String subChannel;
    protected String message;

    public ForwardMessageEvent(String channel, String subChannel, String message) {
        this.channel = channel;
        this.subChannel = subChannel;
        this.message = message;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public String getChannel() {
        return channel;
    }

    public String getSubChannel() {
        return subChannel;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ForwardMessageEvent{" +
                "channel='" + channel + '\'' +
                ", subChannel='" + subChannel + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
