package com.masterpik.api.commands;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.*;
import com.masterpik.api.Main;
import com.masterpik.api.MasterpikAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;

public class Crash implements CommandExecutor {

    private static HashMap<String, Player> crashers;
    public static ArrayList<Player> crash;


    public static void init() {
        crashers = new HashMap<>();
        crash = new ArrayList<>();

        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(Main.plugin, PacketType.Play.Server.getInstance().values()) {
            @Override
            public void onPacketSending(PacketEvent event) {
                if (crash != null
                        && !crash.isEmpty()
                        && crash.size() > 0
                        && crash.contains(event.getPlayer())) {
                    event.setCancelled(true);
                }
            }
        });
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (args != null
                && args.length > 0) {

            Player player = Bukkit.getPlayer(args[0]);

            if (player != null) {

                if (crashers.containsKey(sender.getName())
                        && crashers.get(sender.getName()).equals(player)) {
                    crash.add(player);
                    sender.sendMessage("player " + args[0] + " will crash soon");
                    return true;
                } else {
                    crashers.put(sender.getName(), player);
                    sender.sendMessage("WARNING crash a player is dangerous, to confirm resend command : /" + label + " " + args[0]);
                    return true;
                }


            } else {
                sender.sendMessage("player " + args[0] + " is no connected");
                return false;
            }

        } else {
            sender.sendMessage("enter a name");
            return false;
        }
    }

}
