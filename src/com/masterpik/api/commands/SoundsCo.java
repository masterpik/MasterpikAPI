package com.masterpik.api.commands;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SoundsCo implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            if (command.getName().equalsIgnoreCase("soundplay")) {
                if (args.length >= 4) {

                    Player player = Bukkit.getPlayer(args[0]);

                    if (player != null) {

                        Sound sound = null;

                        for (Sound s : Sound.values()) {
                            if (s.toString().equalsIgnoreCase(args[1])) {
                                sound = s;
                                break;
                            }
                        }

                        if (sound != null) {

                            float volume = -1;

                            try {
                                volume = Float.parseFloat(args[2]);
                            } catch (NumberFormatException e) {
                                return false;
                            }

                            if (volume != -1) {

                                float pitch = -1;

                                try {
                                    pitch = Float.parseFloat(args[2]);
                                } catch (NumberFormatException e) {
                                    return false;
                                }

                                if (pitch != -1) {

                                    player.playSound(player.getLocation(), sound, volume, pitch);
                                    return true;

                                }

                            }

                        } else {
                            return false;
                        }

                    } else {
                        return false;
                    }

                } else {
                    return false;
                }
            }

        } else {
            return false;
        }

        return false;
    }
}
