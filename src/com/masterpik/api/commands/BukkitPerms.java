package com.masterpik.api.commands;

import com.masterpik.api.Main;
import com.masterpik.api.util.UtilPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;

public class BukkitPerms implements CommandExecutor {

    public static void init() {
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (args != null
                && args.length >= 2) {

            Player player = Bukkit.getPlayer(args[0]);

            if (player != null && player.isValid()) {

                String perm = args[1];

                if (player.hasPermission(perm)) {
                    UtilPlayer.removePermission(player, perm);
                    sender.sendMessage("- remove perm ["+perm+"] to player ["+player.getName()+"]");
                } else {
                    UtilPlayer.addPermission(player, perm);
                    sender.sendMessage("+ add perm ["+perm+"] to player ["+player.getName()+"]");
                }

                return true;
            }

        }

        return false;
    }
}
