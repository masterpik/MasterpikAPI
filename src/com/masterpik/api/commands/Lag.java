package com.masterpik.api.commands;

import com.masterpik.api.util.UtilNumbers;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.api.util.UtilServer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Lag implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        double[] pings = UtilServer.getRecentTps();

        double m1 = UtilNumbers.arround(pings[0], 2);
        double m5 = UtilNumbers.arround(pings[1], 2);
        double m15 = UtilNumbers.arround(pings[2], 2);

        if (sender instanceof Player) {

            sender.sendMessage("PING : §l"+ UtilPlayer.getPing((Player) sender)+"§rms");
            sender.sendMessage("TPS Server : (1m : §l"+m1+"§r | 5m : §l"+m5+"§r | 15m : §l"+m15+"§r)");

        } else {
            sender.sendMessage("Derniers TPS du serveur : (1m : "+m1+" | 5m : "+m5+" | 15m : "+m15+")");
        }
        return true;
    }
}
