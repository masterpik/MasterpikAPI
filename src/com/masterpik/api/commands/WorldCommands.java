package com.masterpik.api.commands;

import com.masterpik.api.util.UtilWorld;
import com.sun.scenario.Settings;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class WorldCommands implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (true/*player.getName().equals("axroy") || player.getName().equals("ShooKenD") || player.getName().equals("Sultan138")*/) {

                int nbArgs = args.length;

                if (command.getName().equalsIgnoreCase("wtp")) {


                    if (nbArgs == 0) {
                        player.sendMessage("need a world to tp");

                        return false;
                    }
                    else if (nbArgs == 1) {
                        String WorldN = args[0];

                        if (UtilWorld.isWorldExist(WorldN)) {
                            World world = Bukkit.getWorld(WorldN);

                            player.sendMessage("teleport to "+world.getName());
                            player.teleport(world.getSpawnLocation());

                            return true;
                        } else {
                            player.sendMessage("The world doesnt exist, try to load it /wload <world>");

                            return false;
                        }

                    }

                }
                else if (command.getName().equalsIgnoreCase("wload")) {
                    if (nbArgs == 0) {
                        player.sendMessage("need a world to load");

                        return false;
                    }
                    else if (nbArgs == 1) {
                        String WorldN = args[0];


                        if (UtilWorld.loadWorldR(WorldN)) {
                            player.sendMessage("The world "+WorldN+" was succefully loaded");
                            return true;
                        } else {
                            player.sendMessage("The world can't load, maybe is not exist, or see in the console");
                            return false;
                        }

                    }
                }
                else if (command.getName().equalsIgnoreCase("wunload")) {
                    if (nbArgs == 0) {
                        player.sendMessage("need a world to unload");

                        return false;
                    }
                    else if (nbArgs >= 1) {
                        String WorldN = args[0];

                        if (UtilWorld.isWorldExist(WorldN)) {
                            int bucle = 0;

                            ArrayList<Player> playersInWorld = new ArrayList<>();

                            playersInWorld.addAll(Bukkit.getWorld(WorldN).getPlayers());

                            while (bucle < playersInWorld.size()) {

                                playersInWorld.get(bucle).teleport(Bukkit.getWorlds().get(0).getSpawnLocation());

                                bucle++;
                            }
                        }

                        boolean bool = true;

                        if (nbArgs >= 2) {
                            bool = Boolean.getBoolean(args[1]);
                        }

                        if (UtilWorld.unloadWorldR(WorldN, bool)) {



                            player.sendMessage("The world "+WorldN+" was succefully unload, save : "+Boolean.toString(bool));
                            return true;
                        } else {
                            player.sendMessage("The world can't unload, maybe is not exist, or see in the console");
                            return false;
                        }

                    }
                } else if (command.getName().equalsIgnoreCase("wlist")) {

                    player.sendMessage("world list :");

                    int bucle = 0;

                    ArrayList<World> worlds = new ArrayList<>();

                    worlds.addAll(Bukkit.getWorlds());

                    while (bucle < worlds.size()) {

                        player.sendMessage(""+(bucle+1)+"- "+worlds.get(bucle).getName());

                        bucle++;
                    }
                    return true;
                }
                else {

                    return false;
                }

            }

        } else {
            return false;
        }

        return false;
    }
}
