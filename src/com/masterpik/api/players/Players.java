package com.masterpik.api.players;

import com.masterpik.api.Main;
import com.masterpik.api.MasterpikAPI;
import com.masterpik.api.players.status.Statu;
import com.masterpik.api.players.status.StatusManagement;
import com.masterpik.bungeecommands.party.Party;
import com.masterpik.bungeecommands.party.PartyManagement;
import com.masterpik.database.api.Query;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

public class Players {

    public static HashMap<UUID, MasterpikPlayer> players;

    public static void playersInit() {
        players = new HashMap<>();

        if (MasterpikAPI.isSpigot) {
            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        playerJoin(player.getUniqueId());
                    }
                }
            }, 100);
        }
    }

    public static void playerJoin(UUID uuid) {
        players.put(uuid, new MasterpikPlayer(uuid, getPlayerStatu(uuid), getPlayerParty(uuid)));
    }

    public static Statu getPlayerStatu(UUID uuid) {


        try {
            String query1 = "SELECT status FROM masterpik.players WHERE uuid = '"+ uuid + "'";
            //Bukkit.getLogger().info("QUERY :::: "+query1);
            ResultSet result1 = Query.queryOutpout(MasterpikAPI.connection, query1);

            result1.next();
            byte status = result1.getByte("status");
            return StatusManagement.status.get(status);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static Byte getPlayerParty(UUID uuid) {

        try {

            String query1 = "SELECT party FROM masterpik.players WHERE uuid = '"+ uuid + "'";
            //Bukkit.getLogger().info("QUERY :::: "+query1);
            ResultSet result1 = Query.queryOutpout(MasterpikAPI.connection, query1);

            result1.next();
            return result1.getByte("party");
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

    }

}
