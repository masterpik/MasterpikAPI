package com.masterpik.api.players;

import com.masterpik.api.Main;
import com.masterpik.api.MasterpikAPI;
import com.masterpik.api.players.status.Statu;
import com.masterpik.api.players.status.StatusManagement;
import com.masterpik.api.util.UtilPlayer;
import com.masterpik.bungeecommands.party.Party;
import com.masterpik.bungeecommands.party.PartyManagement;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class MasterpikPlayer {

    protected UUID uuid;

    protected Statu statu;

    protected Party party;
    protected byte partyId;

    public MasterpikPlayer(UUID uuid) {
        this.uuid = uuid;
        this.statu = (Statu) StatusManagement.status.values().toArray()[0];
        this.partyId = -1;
    }

    public MasterpikPlayer(UUID uuid, Statu statu) {
        this.uuid = uuid;
        this.statu = statu;
        this.party = null;
        this.partyId = -1;
    }

    public MasterpikPlayer(UUID uuid, Statu statu, byte party) {
        this.uuid = uuid;
        if (statu != null) {
            this.statu = statu;
        } else if (StatusManagement.status != null
                && StatusManagement.status.size() > 0
                && !StatusManagement.status.isEmpty()){
            this.statu = (Statu) StatusManagement.status.values().toArray()[0];
        } else {
            this.statu = null;
        }

        this.partyId = party;

        if (party != -1 && !MasterpikAPI.isSpigot) {
            this.party = PartyManagement.partys.get(party);
        } else {
            this.party = null;
        }
    }

    public MasterpikPlayer(MasterpikPlayer clone) {
        this.uuid = clone.getUuid();
        this.statu = clone.getStatu();
        this.party = clone.getParty();
        this.partyId = clone.getPartyId();
    }



    public UUID getUuid() {
        return uuid;
    }
    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Statu getStatu() {
        return statu;
    }
    public void setStatu(Statu statu) {
        this.statu = statu;
    }

    public Party getParty() {
        return party;
    }
    public void setParty(Party party) {
        this.party = party;
    }

    public byte getPartyId() {
        return partyId;
    }
    public void setPartyId(byte partyId) {
        this.partyId = partyId;
    }

    public Object getPlayer() {
        if (MasterpikAPI.isSpigot) {
            return Bukkit.getPlayer(this.uuid);
        } else {
            return ProxyServer.getInstance().getPlayer(this.uuid);
        }
    }
    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(this.uuid);
    }
    public ProxiedPlayer getBungeePlayer() {
        return ProxyServer.getInstance().getPlayer(this.uuid);
    }

    public int getPing() {
        if (MasterpikAPI.isSpigot) {
            return UtilPlayer.getPing(this.getBukkitPlayer());
        }
        else {
            return -1;
        }
    }
}
