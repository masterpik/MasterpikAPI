package com.masterpik.api.players.status;

import com.masterpik.api.texts.Colors;

public class Statu {

    protected byte id;

    protected String name;

    protected boolean nameVisible;

    protected Colors color;

    //TODO price

    protected boolean shopVisible;

    protected boolean chooseTeam;

    protected boolean createParty;

    protected byte maxPartyPlayer;

    protected boolean havePet;

    //TODO object petPack:
    protected byte petPack;

    protected boolean hubArmor;

    protected boolean hubHead;

    protected boolean hubBanner;

    protected boolean hubElytra;

    protected boolean manageTeam;

    protected boolean playAlpha;

    protected boolean playBeta;

    protected boolean showHubCo;



    public Statu(byte id) {
        this.id = id;

        this.name = null;
        this.nameVisible = false;
        this.color = null;
        this.shopVisible = false;
        this.chooseTeam = false;
        this.createParty = false;
        this.maxPartyPlayer = -1;
        this.havePet = false;
        this.petPack = -1;
        this.hubArmor = false;
        this.hubHead = false;
        this.hubBanner = false;
        this.hubElytra = false;
        this.manageTeam = false;
        this.playAlpha = false;
        this.playBeta = false;
        this.showHubCo = false;
    }

    public Statu(byte id, String name, boolean nameVisible, Colors color, boolean shopVisible, boolean chooseTeam, boolean createParty, byte maxPartyPlayer, boolean havePet, byte petPack, boolean hubArmor, boolean hubHead, boolean hubBanner, boolean hubElytra, boolean manageTeam, boolean playAlpha, boolean playBeta, boolean showHubCo) {
        this.id = id;
        this.name = name;
        this.nameVisible = nameVisible;
        this.color = color;
        this.shopVisible = shopVisible;
        this.chooseTeam = chooseTeam;
        this.createParty = createParty;
        this.maxPartyPlayer = maxPartyPlayer;
        this.havePet = havePet;
        this.petPack = petPack;
        this.hubArmor = hubArmor;
        this.hubHead = hubHead;
        this.hubBanner = hubBanner;
        this.hubElytra = hubElytra;
        this.manageTeam = manageTeam;
        this.playAlpha = playAlpha;
        this.playBeta = playBeta;
        this.showHubCo = showHubCo;
    }

    public byte getId() {
        return id;
    }
    public Statu setId(byte id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }
    public Statu setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isNameVisible() {
        return nameVisible;
    }
    public Statu setNameVisible(boolean nameVisible) {
        this.nameVisible = nameVisible;
        return this;
    }

    public Colors getColor() {
        return color;
    }
    public Statu setColor(Colors color) {
        this.color = color;
        return this;
    }

    public boolean isShopVisible() {
        return shopVisible;
    }
    public Statu setShopVisible(boolean shopVisible) {
        this.shopVisible = shopVisible;
        return this;
    }

    public boolean isChooseTeam() {
        return chooseTeam;
    }
    public Statu setChooseTeam(boolean chooseTeam) {
        this.chooseTeam = chooseTeam;
        return this;
    }

    public boolean isCreateParty() {
        return createParty;
    }
    public Statu setCreateParty(boolean createParty) {
        this.createParty = createParty;
        return this;
    }

    public byte getMaxPartyPlayer() {
        return maxPartyPlayer;
    }
    public Statu setMaxPartyPlayer(byte maxPartyPlayer) {
        this.maxPartyPlayer = maxPartyPlayer;
        return this;
    }

    public boolean isHavePet() {
        return havePet;
    }
    public Statu setHavePet(boolean havePet) {
        this.havePet = havePet;
        return this;
    }

    public byte getPetPack() {
        return petPack;
    }
    public Statu setPetPack(byte petPack) {
        this.petPack = petPack;
        return this;
    }

    public boolean isHubArmor() {
        return hubArmor;
    }
    public Statu setHubArmor(boolean hubArmor) {
        this.hubArmor = hubArmor;
        return this;
    }

    public boolean isHubHead() {
        return hubHead;
    }
    public Statu setHubHead(boolean hubHead) {
        this.hubHead = hubHead;
        return this;
    }

    public boolean isHubBanner() {
        return hubBanner;
    }
    public Statu setHubBanner(boolean hubBanner) {
        this.hubBanner = hubBanner;
        return this;
    }

    public boolean isHubElytra() {
        return hubElytra;
    }
    public Statu setHubElytra(boolean hubElytra) {
        this.hubElytra = hubElytra;
        return this;
    }

    public boolean isManageTeam() {
        return manageTeam;
    }
    public Statu setManageTeam(boolean manageTeam) {
        this.manageTeam = manageTeam;
        return this;
    }

    public boolean isPlayAlpha() {
        return playAlpha;
    }
    public Statu setPlayAlpha(boolean playAlpha) {
        this.playAlpha = playAlpha;
        return this;
    }

    public boolean isPlayBeta() {
        return playBeta;
    }
    public Statu setPlayBeta(boolean playBeta) {
        this.playBeta = playBeta;
        return this;
    }

    public boolean isShowHubCo() {
        return showHubCo;
    }
    public Statu setShowHubCo(boolean showHubCo) {
        this.showHubCo = showHubCo;
        return this;
    }


}
