package com.masterpik.api.players.status;

import com.masterpik.api.MasterpikAPI;
import com.masterpik.api.util.UtilColor;
import com.masterpik.database.api.Query;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;

public class StatusManagement {

    public static HashMap<Byte, Statu> status;

    public static void StatusInit() {

        status = new HashMap<>();



        try {
            String query1 = "SELECT * FROM masterpik.status";
            //Bukkit.getLogger().info("QUERY :::: "+query1);
            ResultSet result1 = Query.queryOutpout(MasterpikAPI.connection, query1);

            ResultSetMetaData metaData1 = result1.getMetaData();

            while(result1.next()) {

                Statu statu = new Statu(result1.getByte("id"))
                        .setName(result1.getString("name"))
                        .setNameVisible(result1.getBoolean("isNameVisible"))
                        .setColor(UtilColor.chatColorStringToColors(result1.getString("color")))
                        .setShopVisible(result1.getBoolean("isShopVisible"))
                        .setChooseTeam(result1.getBoolean("canChooseGameTeam"))
                        .setCreateParty(result1.getBoolean("canCreateParty"))
                        .setMaxPartyPlayer(result1.getByte("maxPartySize"))
                        .setHavePet(result1.getBoolean("canHavePet"))
                        .setPetPack(result1.getByte("petPack"))
                        .setHubArmor(result1.getBoolean("canHaveArmor"))
                        .setHubHead(result1.getBoolean("canHaveHead"))
                        .setHubBanner(result1.getBoolean("canHaveBanner"))
                        .setHubElytra(result1.getBoolean("canHaveElytra"))
                        .setManageTeam(result1.getBoolean("canManageTeam"))
                        .setPlayAlpha(result1.getBoolean("canPlayAlpha"))
                        .setPlayBeta(result1.getBoolean("canPlayBeta"))
                        .setShowHubCo(result1.getBoolean("isShowCo"));

                status.put(statu.getId(), statu);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
