package com.masterpik.api.teamspeak;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.ChannelProperty;

import java.util.HashMap;

public class Teamspeak {

    private static TS3Config config;
    private static TS3Query query;

    private static TS3Api api;

    public static void init() {
        //connect();
    }

    public static void connect() {
        config = new TS3Config();
        config.setHost("ts.masterpik.com");
        config.setLoginCredentials("MasterpikBot", "Nl5rcA+N");

        query = new TS3Query(config);
        query.connect();

        api = new TS3Api(query);
        api.selectVirtualServerById(1);
        api.setNickname("MasterpikBot");
    }

    public static void exit() {
        if (query != null) {
            query.exit();
        }
    }

    public static TS3Config getConfig() {
        return config;
    }
    public static void setConfig(TS3Config config) {
        Teamspeak.config = config;
    }

    public static TS3Query getQuery() {
        return query;
    }
    public static void setQuery(TS3Query query) {
        Teamspeak.query = query;
    }

    public static TS3Api getApi() {
        return api;
    }
    public static void setApi(TS3Api api) {
        Teamspeak.api = api;
    }
}
