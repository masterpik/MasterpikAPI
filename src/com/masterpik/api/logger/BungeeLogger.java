package com.masterpik.api.logger;

import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilColor;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeeLogger {

    CommandSender expeditor;
    Colors color;


    public BungeeLogger(CommandSender expeditor) {
        this.expeditor = expeditor;
        this.color = Colors.WHITE;
    }

    public BungeeLogger(CommandSender expeditor, Colors color) {
        this.expeditor = expeditor;
        this.color = color;
    }

    public void info(String message) {
        BaseComponent text = new TextComponent(message);
        if (expeditor instanceof ProxiedPlayer) {

            text.setColor(ChatColor.getByChar(this.color.getCode()));

        }


        expeditor.sendMessage(text);
    }


    public CommandSender getExpeditor() {
        return expeditor;
    }

    public BungeeLogger setExpeditor(CommandSender expeditor) {
        this.expeditor = expeditor;
        return this;
    }

    public Colors getColor() {
        return color;
    }

    public BungeeLogger setColor(Colors color) {
        this.color = color;
        return this;
    }
}
