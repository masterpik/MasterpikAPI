package com.masterpik.api.specialEffects;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

public class Fireworks {

    public static void explodeFirework(Location location, int occase, Color color, boolean trail, boolean flicker) {

        FireworkEffect.Type type = FireworkEffect.Type.BALL;

        if (occase == 1) {
            type = FireworkEffect.Type.BALL;
        }
        else if (occase == 2) {
            type = FireworkEffect.Type.BALL_LARGE;
        }
        else if (occase == 3) {
            type = FireworkEffect.Type.CREEPER;
        }


        Location explosing = location.clone();
        explosing.add(0, 1, 0);

        Firework firework = explosing.getWorld().spawn(explosing, Firework.class);

        FireworkEffect effect = FireworkEffect.builder().trail(trail).flicker(flicker).withColor(color).with(type).build();

        FireworkMeta fireworkMeta = firework.getFireworkMeta();

        fireworkMeta.clearEffects();
        fireworkMeta.addEffect(effect);
        Field field;

        try {
            field = fireworkMeta.getClass().getDeclaredField("power");
            field.setAccessible(true);
            field.set(fireworkMeta, -1);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        firework.setFireworkMeta(fireworkMeta);

    }

    public static void explodeFirework(Location location, FireworkEffect.Type type, ArrayList<Color> color,  ArrayList<Color> fadeColor, boolean trail, boolean flicker) {


        Location explosing = location.clone();
        explosing.add(0, 1, 0);

        Firework firework = explosing.getWorld().spawn(explosing, Firework.class);

        FireworkEffect effect = FireworkEffect.builder().trail(trail).flicker(flicker).withColor((Iterable<Color>)color).with(type).withFade((Iterable<Color>)fadeColor).build();

        FireworkMeta fireworkMeta = firework.getFireworkMeta();

        fireworkMeta.clearEffects();
        fireworkMeta.addEffect(effect);
        Field field;

        try {
            field = fireworkMeta.getClass().getDeclaredField("power");
            field.setAccessible(true);
            field.set(fireworkMeta, -1);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        firework.setFireworkMeta(fireworkMeta);

    }

}
