package com.masterpik.api.Networks;

public enum DatasPing {

    MOTD(0),
    ONLINE_PLAYERS(1),
    MAX_PLAYERS(2);

    private int dataPos;

    DatasPing(int dataPos) {
        this.dataPos = dataPos;
    }

    public int getDataPos() {
        return dataPos;
    }

    @Override
    public String toString() {
        return "DatasPing{" +
                "dataPos=" + dataPos +
                '}';
    }
}
