package com.masterpik.api.Networks;

public enum Servers {
    PROXY("proxy", ServerType.BUNGEECORD, "62.210.136.149", 25565),
    HUB("hub", ServerType.SPIGOT, "172.17.0.1", 25501),
    RUSH("rush", ServerType.SPIGOT, "172.17.0.1", 25502),
    TOWER("tower", ServerType.SPIGOT, "172.17.0.1", 25503),
    JUMPSRUN("jumpsrun", ServerType.SPIGOT, "172.17.0.1", 25504);

    private String name;
    private ServerType type;
    private String ip;
    private int port;

    Servers(String name, ServerType type, String ip, int port) {
        this.name = name;
        this.type = type;
        this.ip = ip;
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public ServerType getType() {
        return type;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Servers{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }
}
