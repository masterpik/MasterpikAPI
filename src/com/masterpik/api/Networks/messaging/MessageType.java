package com.masterpik.api.Networks.messaging;

import java.util.HashMap;

public enum MessageType {

    CONNECT("Connect"),
    FORWARD("Forward");/*,
    CONNECT_OTHER("ConnectOther"),
    IP("IP"),
    PLAYER_COUNT("PlayerCount"),
    PLAYER_LIST("PlayerList"),
    GET_SERVERS("GetServers"),
    MESSAGE("Message"),
    GET_SERVER("GetServer"),
    UUID("UUID"),
    UUID_OTHER("UUIDOther");*/

    private String name;

    MessageType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
