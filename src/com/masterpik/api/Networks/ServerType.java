package com.masterpik.api.Networks;

public enum ServerType {
    BUNGEECORD(),
    SPIGOT(),
    BUKKIT();

    ServerType() {
    }

    @Override
    public String toString() {
        return "ServerType{}";
    }
}
