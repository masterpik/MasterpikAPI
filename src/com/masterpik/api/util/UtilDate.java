package com.masterpik.api.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

public class UtilDate {

    public static int millisecondsToMinutes(long millis) {
        int re = (int)(millis / 60000L % 60L);

        if (re == 0) re = 1;

        return re;
    }

    public static Date getCurrentDate() {
        return new Date(Calendar.getInstance().getTimeInMillis());
    }

    public static Timestamp getCurrentTimeStamp() {
        return new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

    public static Date addToDate(Date date, int calendarType, int nb) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(calendarType, nb);
        return new Date(calendar.getTimeInMillis());
    }

    public static Timestamp addToTimeStamp(Timestamp date, int calendarType, int nb) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(calendarType, nb);
        return new Timestamp(calendar.getTimeInMillis());
    }

    public static int getCalendarTypeFromString(String type) {
        if (type.equalsIgnoreCase("D")
                || type.equalsIgnoreCase("J")
                || type.equalsIgnoreCase("days")
                || type.equalsIgnoreCase("day")
                || type.equalsIgnoreCase("jour")
                || type.equalsIgnoreCase("jours")) {
            return Calendar.DAY_OF_MONTH;
        } else if(type.equalsIgnoreCase("M")
                || type.equalsIgnoreCase("months")
                || type.equalsIgnoreCase("month")
                || type.equalsIgnoreCase("mois")
                || type.equalsIgnoreCase("moi")) {
            return Calendar.MONTH;
        } else if(type.equalsIgnoreCase("Y")
                || type.equalsIgnoreCase("A")
                || type.equalsIgnoreCase("years")
                || type.equalsIgnoreCase("year")
                || type.equalsIgnoreCase("an")
                || type.equalsIgnoreCase("ans")) {
            return Calendar.YEAR;
        } else if(type.equalsIgnoreCase("H")
                || type.equalsIgnoreCase("hours")
                || type.equalsIgnoreCase("hour")
                || type.equalsIgnoreCase("heures")
                || type.equalsIgnoreCase("heure")) {
            return Calendar.HOUR_OF_DAY;
        } else if(type.equalsIgnoreCase("Mi")
                || type.equalsIgnoreCase("minutes")
                || type.equalsIgnoreCase("minute")) {
            return Calendar.MINUTE;
        }
        else {
            return -1;
        }
    }

}
