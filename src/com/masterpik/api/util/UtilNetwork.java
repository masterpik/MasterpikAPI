package com.masterpik.api.util;

import com.masterpik.api.Networks.DatasPing;
import com.masterpik.api.Networks.Servers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.rmi.UnknownHostException;

public class UtilNetwork {

    public static long ip2LongNotCompressed(String ipAddress) {
        long result = 0;
        String[] atoms = ipAddress.split("\\.");

        for (int i = 3; i >= 0; i--) {
            result |= (Long.parseLong(atoms[3 - i]) << (i * 8));
        }

        return result & 0xFFFFFFFF;
    }

    public static int ip2longCompressed(String ipAdress) {
        long result = 0;

        result = ip2LongNotCompressed(ipAdress);

        //Compress to SQL Integer - 2147483648

        result = result - 2147483648L;

        return (int) result;
    }

    public static String long2Ip(long ip) {
        StringBuilder sb = new StringBuilder(15);

        for (int i = 0; i < 4; i++) {
            sb.insert(0, Long.toString(ip & 0xff));

            if (i < 3) {
                sb.insert(0, '.');
            }

            ip >>= 8;
        }

        return sb.toString();
    }

    public static String longCompressed2Ip(int ip) {
        return long2Ip(ip+2147483648L);
    }

    public static boolean serverIsOnline(String ip, int port) {
        Socket socket = null;

        try {
            socket = new Socket(InetAddress.getByName(ip), port) ;
            socket.setSoTimeout(10000);
            socket.close();
        } catch (UnknownHostException e) {
            return false;
        } catch (IOException e) {
            return false;
        }

        return socket.isConnected();

    }
    public static boolean serverIsOnline(Servers server) {
        Socket socket = null;

        try {
            socket = new Socket(InetAddress.getByName(server.getIp()), server.getPort()) ;
            socket.setSoTimeout(10000);
            socket.close();
        } catch (UnknownHostException e) {
            return false;
        } catch (IOException e) {
            return false;
        }

        return socket.isConnected();

    }

    public static String getDatasServer(String ip, int port, DatasPing type) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(ip, port), 1 * 1000);

            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            DataInputStream in = new DataInputStream(socket.getInputStream());

            out.write(0xFE);

            StringBuilder str = new StringBuilder();

            int b;
            while ((b = in.read()) != -1) {
                if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
                    str.append((char) b);
                }
            }

            String[] data = str.toString().split("§");
            /*String motd = data[0];
            int onlinePlayers = Integer.valueOf(data[1]);
            int maxPlayers = Integer.valueOf(data[2]);*/

            return data[type.getDataPos()];

            //System.out.println("The server's MOTD is '" + motd + "'. The player count is " + onlinePlayers + "/" + maxPlayers);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDatasServer(Servers server, DatasPing type) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(server.getIp(), server.getPort()), 1 * 1000);

            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            DataInputStream in = new DataInputStream(socket.getInputStream());

            out.write(0xFE);

            StringBuilder str = new StringBuilder();

            int b;
            while ((b = in.read()) != -1) {
                if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
                    str.append((char) b);
                }
            }

            String[] data = str.toString().split("§");
            /*String motd = data[0];
            int onlinePlayers = Integer.valueOf(data[1]);
            int maxPlayers = Integer.valueOf(data[2]);*/

            return data[type.getDataPos()];

            //System.out.println("The server's MOTD is '" + motd + "'. The player count is " + onlinePlayers + "/" + maxPlayers);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean checkIp(String ip) {
        String[] nbIp = ip.split("\\.");

        return nbIp.length == 4
                && (Integer.parseInt(nbIp[0]) <= 255 && Integer.parseInt(nbIp[0]) >= 0)
                && (Integer.parseInt(nbIp[1]) <= 255 && Integer.parseInt(nbIp[1]) >= 0)
                && (Integer.parseInt(nbIp[2]) <= 255 && Integer.parseInt(nbIp[2]) >= 0)
                && (Integer.parseInt(nbIp[3]) <= 255 && Integer.parseInt(nbIp[3]) >= 0);
    }

}
