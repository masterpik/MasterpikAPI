package com.masterpik.api.util;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class UtilBanners {


    @Deprecated
    public static void setMasterpik(Banner banner) {

    }

    public static void resetBanner(Banner banner) {
        int bucle = banner.numberOfPatterns();

        while (bucle > 0) {

            banner.removePattern(bucle-1);

            bucle --;
        }

        banner.update();
    }

    public static void setNumberOnBanner(Banner banner, int number) {

        DyeColor color = DyeColor.WHITE;
        DyeColor bannerColor = banner.getBaseColor();

        resetBanner(banner);

        switch (number)
        {
            case 0:
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_RIGHT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_LEFT));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 1:
                banner.addPattern(new Pattern(color, PatternType.SQUARE_TOP_LEFT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_CENTER));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 2:
                /*banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_LEFT));
                banner.addPattern(new Pattern(bannerColor, PatternType.HALF_HORIZONTAL));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));*/

                banner.addPattern(new Pattern(color, PatternType.TRIANGLE_TOP));
                banner.addPattern(new Pattern(color, PatternType.TRIANGLE_BOTTOM));
                banner.addPattern(new Pattern(color, PatternType.SQUARE_TOP_LEFT));
                banner.addPattern(new Pattern(color, PatternType.SQUARE_BOTTOM_RIGHT));
                banner.addPattern(new Pattern(bannerColor, PatternType.RHOMBUS_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_DOWNLEFT));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 3:
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_RIGHT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_MIDDLE));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 4:
                banner.addPattern(new Pattern(color, PatternType.HALF_HORIZONTAL));
                banner.addPattern(new Pattern(bannerColor, PatternType.STRIPE_CENTER));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_RIGHT));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 5:
                /*banner.setBaseColor(color);
                banner.addPattern(new Pattern(bannerColor, PatternType.HALF_VERTICAL));
                banner.addPattern(new Pattern(bannerColor, PatternType.HALF_HORIZONTAL));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                banner.setBaseColor(bannerColor);*/

                /*banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));
                banner.addPattern(new Pattern(bannerColor, PatternType.RHOMBUS_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_DOWNRIGHT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));*/

                banner.addPattern(new Pattern(color, PatternType.TRIANGLE_TOP));
                banner.addPattern(new Pattern(color, PatternType.TRIANGLE_BOTTOM));
                banner.addPattern(new Pattern(color, PatternType.SQUARE_TOP_LEFT));
                banner.addPattern(new Pattern(color, PatternType.SQUARE_BOTTOM_RIGHT));
                banner.addPattern(new Pattern(bannerColor, PatternType.RHOMBUS_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_DOWNRIGHT));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));

                break;
            case 6:
                banner.addPattern(new Pattern(color, PatternType.STRIPE_RIGHT));
                banner.addPattern(new Pattern(bannerColor, PatternType.HALF_HORIZONTAL));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_LEFT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));

                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));

                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 7:
                /*banner.addPattern(new Pattern(color, PatternType.STRIPE_RIGHT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));*/

                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(bannerColor, PatternType.DIAGONAL_RIGHT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_DOWNLEFT));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));

                break;
            case 8:
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_BOTTOM));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_RIGHT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_LEFT));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 9:
                banner.addPattern(new Pattern(color, PatternType.HALF_HORIZONTAL));
                banner.addPattern(new Pattern(bannerColor, PatternType.STRIPE_CENTER));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_TOP));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_RIGHT));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            case 10:
                banner.addPattern(new Pattern(color, PatternType.STRIPE_DOWNLEFT));
                banner.addPattern(new Pattern(color, PatternType.STRIPE_DOWNRIGHT));
                banner.addPattern(new Pattern(bannerColor, PatternType.RHOMBUS_MIDDLE));
                banner.addPattern(new Pattern(color, PatternType.CROSS));
                banner.addPattern(new Pattern(bannerColor, PatternType.BORDER));
                break;
            default:
                banner = banner;
        }

        banner.update();

    }


}
