package com.masterpik.api.util;

import org.bukkit.DyeColor;
import org.bukkit.block.Banner;
import org.bukkit.block.banner.Pattern;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;
import java.util.List;

public class UtilShield {

    public static ItemStack bannerOnShield(ItemStack shield, Banner banner) {
        return patternOnShield(shield, banner.getBaseColor(), banner.getPatterns());
    }

    public static ItemStack patternOnShield(ItemStack shield, DyeColor baseColor, List<Pattern> paterns) {

        ItemMeta meta = shield.getItemMeta();
        BlockStateMeta bmeta = (BlockStateMeta) meta;
        Banner banner = (Banner) bmeta.getBlockState();
        banner.setBaseColor(baseColor);
        if (paterns != null
                && paterns.size() != 0
                && !paterns.isEmpty()) {
            banner.setPatterns(paterns);
        }
        banner.update();
        bmeta.setBlockState(banner);
        shield.setItemMeta(bmeta);

        return shield;
    }

}
