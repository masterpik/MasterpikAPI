package com.masterpik.api.util;

import com.comphenix.packetwrapper.WrapperPlayServerTitle;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.masterpik.api.nms.player.Title;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class UtilTitle {

    public static void sendTitle(Title title, Player player) {
        WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
        packet.setAction(title.getAction());

        WrappedChatComponent text = WrappedChatComponent.fromJson(UtilJson.getJsonFromObject(title.getText()));

        packet.setTitle(text);
        packet.setFadeIn(title.getFadeIn());
        packet.setStay(title.getStay());
        packet.setFadeOut(title.getFadeOut());

        packet.sendPacket(player);


    }

    public static void sendTitle(Title title, ArrayList<Player> players) {

        for (Player pl : players) {
            /*WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
            packet.setAction(title.getAction());

            WrappedChatComponent text = WrappedChatComponent.fromJson(UtilJson.getJsonFromObject(title.getText()));

            packet.setTitle(text);
            packet.setFadeIn(title.getFadeIn());
            packet.setStay(title.getStay());
            packet.setFadeOut(title.getFadeOut());

            packet.sendPacket(pl);*/
            sendTitle(title, pl);
        }
    }

}
