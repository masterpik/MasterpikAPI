package com.masterpik.api.util;

import com.masterpik.api.util.location.Yaws;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class UtilVector {

    public static float getYaw(Vector vector) {
        //return (float) (Math.atan2(vector.getX(), vector.getZ()) * 180.0D / 3.1415927410125732D);
        //return (float) Math.toDegrees(vector.angle(new Vector(0, vector.getY(), 1)));
        return getYawVectors(new Vector(0,0,0), vector);
    }


    public static float getPitch(Vector vector) {
        //return (float) (Math.atan2(vector.getY(), Math.sqrt(vector.getX() * vector.getX() + vector.getZ() * vector.getZ())) * 180.0D / 3.1415927410125732D);
        return getPitchVectors(new Vector(0,0,0), vector);
    }

    public static float getYawVectors(Vector vector1, Vector vector2) {
        return getYawVectors(vector1, vector2, 0);
    }
    public static float getYawVectors(Vector vector1, Vector vector2, float defYaw) {
        Vector A = vector1.clone();
        double XA = A.getX();
        double YA = A.getY();
        YA = YA + 2;
        double ZA = A.getZ();

        Vector B = vector2.clone();
        double XB = B.getX();
        double YB = B.getY();
        YB = YB + 1;
        double ZB = B.getZ();


        double alpha = Math.toDegrees(Math.atan((ZB - ZA) / (XB - XA)));


        float beta = defYaw;

        if (ZB > ZA && XB < XA) {
            beta = (float) (90 + alpha);
        } else if (ZB < ZA && XB < XA) {
            beta = (float) (90 + alpha);
        } else if (ZB < ZA && XB > XA) {
            beta = (float) (-90 + alpha);
        } else if (ZB > ZA && XB > XA) {
            beta = (float) (-90 + alpha);
        } else if (ZB == ZA && XB == XA) {
            beta = beta;
        } else if (ZB == ZA) {
            if (XB > XA) {
                beta = -90;
            } else if (XB < XA) {
                beta = 90;
            }
        } else if (XB == XA) {
            if (ZB > ZA) {
                beta = 0;
            } else if (ZB < ZA) {
                beta = 180;
            }
        }

        return beta;
    }

    public static float getYawLocations(Location location, Location lookAt) {
        /*Location A = location.clone();
        double XA = A.getX();
        double YA = A.getY();
        YA = YA + 2;
        double ZA = A.getZ();

        Location B = lookAt.clone();
        double XB = B.getX();
        double YB = B.getY();
        YB = YB + 1;
        double ZB = B.getZ();


        double alpha = Math.toDegrees(Math.atan((ZB - ZA) / (XB - XA)));


        float beta = A.getYaw();

        if (ZB > ZA && XB < XA) {
            beta = (float) (90 + alpha);
        } else if (ZB < ZA && XB < XA) {
            beta = (float) (90 + alpha);
        } else if (ZB < ZA && XB > XA) {
            beta = (float) (-90 + alpha);
        } else if (ZB > ZA && XB > XA) {
            beta = (float) (-90 + alpha);
        } else if (ZB == ZA && XB == XA) {
            beta = beta;
        } else if (ZB == ZA) {
            if (XB > XA) {
                beta = -90;
            } else if (XB < XA) {
                beta = 90;
            }
        } else if (XB == XA) {
            if (ZB > ZA) {
                beta = 0;
            } else if (ZB < ZA) {
                beta = 180;
            }
        }

        return beta;*/
        return getYawVectors(location.toVector(), lookAt.toVector(), location.getYaw());
    }


    public static float getPitchVectors(Vector vector1, Vector vector2) {
        return getPitchVectors(vector1, vector2, 0);
    }
    public static float getPitchVectors(Vector vector1, Vector vector2, float defPitch) {
        Vector A = vector1.clone();
        double XA = A.getX();
        double YA = A.getY();
        YA = YA + 2;
        double ZA = A.getZ();

        Vector B = vector2.clone();
        double XB = B.getX();
        double YB = B.getY();
        YB = YB + 1;
        double ZB = B.getZ();

        double alpha2 = Math.toDegrees(Math.atan(((YB-YA)/(Math.sqrt(((XB-XA)*(XB-XA))+((ZB-ZA)*(ZB-ZA)))))));

        float beta2 = defPitch;

        if (YB == YA) {
            beta2 = 0;
        } else if (YB >= YA) {
            beta2 = (float) -alpha2;
        } else if (YB <= YA) {
            beta2 = (float) -alpha2;
        }

        if (beta2 > 180) {
            beta2 = 180;
        } else if (beta2 < -180) {
            beta2 = -180;
        }

        return beta2;
    }
    public static float getPitchLocations(Location location , Location lookAt) {
        /*Location A = location.clone();
        double XA = A.getX();
        double YA = A.getY();
        YA = YA + 2;
        double ZA = A.getZ();

        Location B = lookAt.clone();
        double XB = B.getX();
        double YB = B.getY();
        YB = YB + 1;
        double ZB = B.getZ();

        double alpha2 = Math.toDegrees(Math.atan(((YB-YA)/(Math.sqrt(((XB-XA)*(XB-XA))+((ZB-ZA)*(ZB-ZA)))))));

        float beta2 = A.getPitch();

        if (YB == YA) {
            beta2 = 0;
        } else if (YB >= YA) {
            beta2 = (float) -alpha2;
        } else if (YB <= YA) {
            beta2 = (float) -alpha2;
        }

        if (beta2 > 180) {
            beta2 = 180;
        } else if (beta2 < -180) {
            beta2 = -180;
        }

        return beta2;*/
        return getPitchVectors(location.toVector(), lookAt.toVector(), location.getPitch());
    }

    //https://github.com/bergerkiller/BKCommonLib/blob/master/src/main/java/com/bergerkiller/bukkit/common/utils/MathUtil.java
}
