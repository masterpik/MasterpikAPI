package com.masterpik.api.util;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Arrays;

public class UtilSpigot {

    public static ArrayList<Plugin> getServerPluginsListPlugin() {
        ArrayList<Plugin> plugins = new ArrayList<>();
        plugins.addAll(Arrays.asList(Bukkit.getServer().getPluginManager().getPlugins()));
        return plugins;
    }

    public static ArrayList<String> getServerPluginsListString() {
        ArrayList<String> plugins = new ArrayList<>();

        int bucle = 0;

        while (bucle < Bukkit.getServer().getPluginManager().getPlugins().length) {

            plugins.add(Bukkit.getServer().getPluginManager().getPlugins()[bucle].getName());

            bucle++;
        }

        return plugins;
    }


}
