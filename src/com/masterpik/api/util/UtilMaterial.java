package com.masterpik.api.util;

import org.bukkit.Material;

public class UtilMaterial {

    public static boolean isArmor(Material material) {
        boolean is = false;

        switch (material) {

            case LEATHER_HELMET:
            case LEATHER_CHESTPLATE:
            case LEATHER_LEGGINGS:
            case LEATHER_BOOTS:
            case CHAINMAIL_HELMET:
            case CHAINMAIL_CHESTPLATE:
            case CHAINMAIL_LEGGINGS:
            case CHAINMAIL_BOOTS:
            case IRON_HELMET:
            case IRON_CHESTPLATE:
            case IRON_LEGGINGS:
            case IRON_BOOTS:
            case DIAMOND_HELMET:
            case DIAMOND_CHESTPLATE:
            case DIAMOND_LEGGINGS:
            case DIAMOND_BOOTS:
            case GOLD_HELMET:
            case GOLD_CHESTPLATE:
            case GOLD_LEGGINGS:
            case GOLD_BOOTS:
                is = true;
                break;
        }

        return is;
    }

    public static boolean isHelmet(Material material) {
        boolean is = false;

        switch (material) {

            case LEATHER_HELMET:
            case CHAINMAIL_HELMET:
            case IRON_HELMET:
            case DIAMOND_HELMET:
            case GOLD_HELMET:
                is = true;
                break;
        }

        return is;
    }

    public static boolean isChestplate(Material material) {
        boolean is = false;

        switch (material) {

            case LEATHER_CHESTPLATE:
            case CHAINMAIL_CHESTPLATE:
            case IRON_CHESTPLATE:
            case DIAMOND_CHESTPLATE:
            case GOLD_CHESTPLATE:
                is = true;
                break;
        }

        return is;
    }

    public static boolean isLeggings(Material material) {
        boolean is = false;

        switch (material) {

            case LEATHER_LEGGINGS:
            case CHAINMAIL_LEGGINGS:
            case IRON_LEGGINGS:
            case DIAMOND_LEGGINGS:
            case GOLD_LEGGINGS:
                is = true;
                break;
        }

        return is;
    }

    public static boolean isBoots(Material material) {
        boolean is = false;

        switch (material) {

            case LEATHER_BOOTS:
            case CHAINMAIL_BOOTS:
            case IRON_BOOTS:
            case DIAMOND_BOOTS:
            case GOLD_BOOTS:
                is = true;
                break;
        }

        return is;
    }

}
