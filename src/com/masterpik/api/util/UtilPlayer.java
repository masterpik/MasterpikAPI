package com.masterpik.api.util;

import com.masterpik.api.Main;
import com.masterpik.api.MasterpikAPI;
import net.minecraft.server.v1_10_R1.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.UUID;

public class UtilPlayer {

    public static void realPlayerClear(Player player) {
        UtilPlayer.playerInventoryClear(player);
        UtilPlayer.playerArmorsClear(player);
        UtilPlayer.playerXpClear(player);
        UtilPlayer.playerEffectsClear(player);
    }
    public static void playerInventoryClear(Player player) {
        player.getInventory().clear();
    }
    public static void playerArmorsClear(Player player) {
        player.getInventory().setHelmet(new ItemStack(Material.AIR));
        player.getInventory().setChestplate(new ItemStack(Material.AIR));
        player.getInventory().setLeggings(new ItemStack(Material.AIR));
        player.getInventory().setBoots(new ItemStack(Material.AIR));
    }
    public static void playerXpClear(Player player) {
        player.setLevel(0);
        player.setExp(0.0F);
    }
    public static void playerEffectsClear(Player player) {
        for(PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
    }

    public static void showPlayer(Player player, ArrayList<Player> list){
        int bucle = 0;
        while (bucle < list.size()){

            player.showPlayer(list.get(bucle));

            bucle++;
        }
    }

    public static void hidePlayer(Player player, ArrayList<Player> list){
        int bucle = 0;
        while (bucle < list.size()){

            player.hidePlayer(list.get(bucle));

            bucle++;
        }
    }

    public static void setXpBarCount(Player player, int number, float prct) {
        /*float xpNeed = 0;

        float xp1 = UtilExperience.getTotalXpToLvl(number);
        float xp2 = UtilExperience.getTotalXpToLvl(number+1)-1;

        float diff = xp2-xp1;

        xpNeed = xp1 + ((diff*prct)/100);*/

        player.setLevel(number);
        player.setExp(prct/100.0F);

    }

    public static int getPing(Player player) {
        if (MasterpikAPI.isSpigot) {
            CraftPlayer cp = (CraftPlayer) player;
            EntityPlayer ep = cp.getHandle();
            return ep.ping;
        } else {
            return -1;
        }
    }

    @Deprecated
    public static String uuidToPlayerName(UUID uuid) {
        //TODO finish function
        return null;
    }

    @Deprecated
    public static UUID playerNameToUUID(String name) {
        //TODO finish function
        return null;
    }

    public static void addPermission(Player player, String perm) {
        addPermission(player.getUniqueId(), perm);
    }

    public static void addPermission(UUID uuid, String perm) {
        if (Main.perms != null
                && !Main.perms.isEmpty()
                && Main.perms.size() > 0
                && Main.perms.containsKey(uuid)) {
            Main.perms.get(uuid).setPermission(perm, true);
        }
    }

    public static void removePermission(Player player, String perm) {
        removePermission(player.getUniqueId(), perm);
    }

    public static void removePermission(UUID uuid, String perm) {
        if (Main.perms != null
                && !Main.perms.isEmpty()
                && Main.perms.size() > 0
                && Main.perms.containsKey(uuid)) {
            Main.perms.get(uuid).unsetPermission(perm);
        }
    }
}
