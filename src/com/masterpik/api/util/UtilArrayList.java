package com.masterpik.api.util;

import java.util.ArrayList;

public class UtilArrayList {

    public static String ArrayListToCryptedString(ArrayList list) {
        String string = "";

        /*int bucle = 0;

        while (bucle < list.size()) {
            if (bucle == 0) {
                string = ""+bucle+":("+list.get(bucle)+")";
            } else {
                string = ""+string+""+bucle+":("+list.get(bucle)+")";
            }
            bucle++;
        }*/

        int bucle = 0;

        while (bucle < list.size()) {
            if (bucle == 0) {
                string = list.get(bucle).toString();
            } else {
                string = string+":"+list.get(bucle);
            }
            bucle++;
        }

        return string;
    }


    public static <T> ArrayList<T> arryListFromElement(T object) {
        ArrayList<T> list = new ArrayList<>();
        list.add(object);
        return list;
    }

}
