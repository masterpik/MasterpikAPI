package com.masterpik.api.util;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.masterpik.api.Main;
import com.masterpik.api.Networks.Servers;
import com.masterpik.api.Networks.messaging.MessageType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageRecipient;

import java.util.ArrayList;

public class UtilMessaging {

    public static void sendMessage(MessageType type, PluginMessageRecipient sender, String server, String subChannel, String args) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(type.getName());

        switch (type) {
            case CONNECT:
                out.writeUTF(server);
                break;
            case FORWARD:
                final byte[] data = args.getBytes();
                out.writeUTF(server);
                out.writeUTF(subChannel);
                out.writeShort(data.length);
                out.write(data);
                break;
            default:
                break;
        }

        sender.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());

    }

    public static void sendPlayer(Player player, String server) {
        sendMessage(MessageType.CONNECT, player, server, null, null);
    }

    public static void sendForwardMessage(Player player, String server, String subChannel, String datas) {
        sendMessage(MessageType.FORWARD, player, server, subChannel, datas);
    }


    public static void sendGamesCoMessage(Player player, String server, String datas) {
        UtilMessaging.sendForwardMessage(player, server, "gamesCo", datas);
    }

    /*public static void sendGamesCoMessage(Player player, List<String> datas) {
        ArrayList<String> args = new ArrayList<>();
        args.addAll(datas);
        sendGamesCoMessage(player, UtilArrayList.ArrayListToCryptedString(args));
    }*/

    public static void sendGamesCoMessage(Player player, String server, ArrayList<String> datas) {
        datas.add(0, server);
        datas.add(1, player.getName());
        sendGamesCoMessage(player, server, UtilArrayList.ArrayListToCryptedString(datas));
    }

}
