package com.masterpik.api.util;

import com.comphenix.packetwrapper.WrapperPlayServerChat;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.masterpik.api.json.ChatText;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class UtilChat {

    public static void sendChat(String JSON, Player player) {

        WrapperPlayServerChat packet = new WrapperPlayServerChat();
        packet.setMessage(WrappedChatComponent.fromJson(JSON));
        packet.setPosition((byte) 2);

        packet.sendPacket(player);

    }

    public static void sendChat(ChatText text, Player player) {

        WrapperPlayServerChat packet = new WrapperPlayServerChat();
        packet.setMessage(WrappedChatComponent.fromJson(UtilJson.getJsonFromObject(text)));
        packet.setPosition((byte) 2);

        packet.sendPacket(player);

    }

    public static void sendChat(ChatText text, ArrayList<Player> players) {

        for (Player player : players) {
            /*WrapperPlayServerChat packet = new WrapperPlayServerChat();
            packet.setMessage(WrappedChatComponent.fromJson(UtilJson.getJsonFromObject(text)));
            packet.setPosition((byte) 2);
            packet.sendPacket(player);*/
            sendActionBar(text, player);
        }

    }


    public static void sendActionBar(ChatText text, Player player) {

        WrapperPlayServerChat packet = new WrapperPlayServerChat();
        packet.setMessage(WrappedChatComponent.fromJson(UtilJson.getJsonFromObject(text)));
        packet.setPosition((byte) 2);

        packet.sendPacket(player);

    }

    public static void sendActionBar(ChatText text, ArrayList<Player> players) {

        for (Player player : players) {
            /*WrapperPlayServerChat packet = new WrapperPlayServerChat();
            packet.setMessage(WrappedChatComponent.fromJson(UtilJson.getJsonFromObject(text)));
            packet.setPosition((byte) 2);
            packet.sendPacket(player);*/
            sendActionBar(text, player);
        }

    }

}
