package com.masterpik.api.util;

import java.util.ArrayList;

public class UtilInteger {

    public static int addInts(ArrayList<Integer> ints) {
        int result = 0;
        for (int nu : ints) {
            result = result + nu;
        }

        return result;
    }

}
