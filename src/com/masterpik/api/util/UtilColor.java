package com.masterpik.api.util;

import com.masterpik.api.texts.Colors;

public class UtilColor {

    public static String chatColorStringToColorName(String color) {
        return chatColorStringToColors(color).getColor();
    }

    public static Colors chatColorStringToColors(String color) {
        Colors name = Colors.WHITE;

        color = color.replaceAll("§", "");
        color = color.replaceAll("&", "");

        switch (color) {

            case "4":
                name = Colors.DARK_RED;
                break;

            case "c":
                name = Colors.RED;
                break;

            case "6":
                name = Colors.GOLD;
                break;

            case "e":
                name = Colors.YELLOW;
                break;

            case "2":
                name = Colors.DARK_GREEN;
                break;

            case "a":
                name = Colors.GREEN;
                break;

            case "b":
                name = Colors.AQUA;
                break;

            case "3":
                name = Colors.DARK_AQUA;
                break;

            case "1":
                name = Colors.DARK_BLUE;
                break;

            case "9":
                name = Colors.BLUE;
                break;

            case "d":
                name = Colors.LIGHT_PURPLE;
                break;

            case "5":
                name = Colors.DARK_PURPLE;
                break;

            case "f":
                name = Colors.WHITE;
                break;

            case "7":
                name = Colors.GRAY;
                break;

            case "8":
                name = Colors.DARK_GRAY;
                break;

            case "0":
                name = Colors.BLACK;
                break;
            default:
                name = Colors.WHITE;
        }

        return name;
    }

}
