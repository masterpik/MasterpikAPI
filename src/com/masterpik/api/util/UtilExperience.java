package com.masterpik.api.util;

public class UtilExperience {

    public static float getTotalXpToLvl(int lvlToReach) {
        float xp = 0;

        if (lvlToReach >= 0 && lvlToReach <= 16) {
            xp = (lvlToReach*lvlToReach)+(6*lvlToReach);
        } else if(lvlToReach >= 17 && lvlToReach <= 31) {
            xp = (float) ((2.5*(lvlToReach*lvlToReach)) - (40.5*lvlToReach) + 360);
        } else if(lvlToReach >= 32) {
            xp = (float) ((4.5*(lvlToReach*lvlToReach)) - (162.5*lvlToReach) + 2220);
        }

        return xp;
    }

}
