package com.masterpik.api.util;

import com.masterpik.api.Main;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import sun.jvm.hotspot.oops.Array;

import javax.rmi.CORBA.Util;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class UtilWorld{

    public static void cloneWorld(String worldToClone, String clonedWorldName){
        File sourceFolder = new File(Bukkit.getWorldContainer(), worldToClone);
        File targetFolder = new File(Bukkit.getWorldContainer(), clonedWorldName);

        UtilWorld.copyWorldFiles(sourceFolder, targetFolder);

        UtilWorld.loadWorld(clonedWorldName);
    }

    public static void deleteWorld(String worldToDelete) {

        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {
                UtilWorld.unloadWorld(worldToDelete, false);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        File path = new File(Bukkit.getWorldContainer(), worldToDelete);

                        if (UtilWorld.deletWorldFiles(path)) {
                            Bukkit.getLogger().info("[MasterpikAPI] Delete world " + worldToDelete + " succefully");
                        } else {
                            Bukkit.getLogger().warning("[MasterpikAPI] Error when try to delete the world" + worldToDelete);
                        }

                        /*if (path.listFiles() == null || path.listFiles().length != 0) {
                            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                                @Override
                                public void run() {
                                    File path = new File(Bukkit.getWorldContainer(), worldToDelete);

                                    if (UtilWorld.deletWorldFiles(path)) {
                                        Bukkit.getLogger().info("[MasterpikAPI] Delete world " + worldToDelete + " succefully");
                                    } else {
                                        Bukkit.getLogger().warning("[MasterpikAPI] Error when try to delete the world" + worldToDelete);
                                    }


                                }
                            }, 5L);
                        }*/

                    }
                }, 15L);

            }
        }, 5L);

    }

    public static void copyWorldFiles(File source, File target) {
        try {

            ArrayList<String> ignore = new ArrayList<String>(Arrays.asList("uid.dat", "session.dat"));
            if(!ignore.contains(source.getName())) {
                if(source.isDirectory()) {
                    if(!target.exists())
                        target.mkdirs();
                    String files[] = source.list();
                    for (String file : files) {
                        File srcFile = new File(source, file);
                        File destFile = new File(target, file);
                        copyWorldFiles(srcFile, destFile);
                    }
                } else {
                    InputStream in = new FileInputStream(source);
                    OutputStream out = new FileOutputStream(target);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = in.read(buffer)) > 0)
                        out.write(buffer, 0, length);
                    in.close();
                    out.close();
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean deletWorldFiles(File path) {


        if(path.exists()) {
            File files[] = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
                    deletWorldFiles(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return(path.delete());
    }


    public static void loadWorld(String worldToLoad) {
        if (Bukkit.getWorlds().add(Bukkit.getServer().createWorld(new WorldCreator(worldToLoad)))) {
            Bukkit.getLogger().info("[MasterpikAPI] Loaded world " + worldToLoad + " succefully");
        } else {
            Bukkit.getLogger().warning("[MasterpikAPI] Error when try to load the world" + worldToLoad);
        }
    }

    public static boolean loadWorldR(String worldToLoad) {
        if (Bukkit.getWorlds().add(Bukkit.getServer().createWorld(new WorldCreator(worldToLoad)))) {
            Bukkit.getLogger().info("[MasterpikAPI] Loaded world " + worldToLoad + " succefully");
            return true;
        } else {
            Bukkit.getLogger().warning("[MasterpikAPI] Error when try to load the world" + worldToLoad);
            return false;
        }
    }

    public static boolean unloadWorldR(String worldToUnload, boolean save) {

        if (isWorldExist(worldToUnload)) {

            World world = Bukkit.getWorld(worldToUnload);

            ejectPlayers(world.getName(), true);

            /*int bucle = 0;
            ArrayList<Player> players = new ArrayList<>();
            players.addAll(world.getPlayers());
            while (bucle < players.size()) {
                players.get(bucle).performCommand("hub");
                bucle ++;
            }*/

            if (Bukkit.getServer().unloadWorld(world, save)) {
                Bukkit.getLogger().info("[MasterpikAPI] Unload world " + worldToUnload + " succefully");
                return true;
            } else {
                Bukkit.getLogger().warning("[MasterpikAPI] Error when try to unload the world" + worldToUnload);
                return false;
            }
        } else {
            Bukkit.getLogger().warning("[MasterpikAPI] World to unload do not exist");
            return false;
        }
    }

    public static void unloadWorld(String worldToUnload, boolean save) {

        boolean ok = unloadWorldR(worldToUnload, save);

        /*if (isWorldExist(worldToUnload)) {

            World world = Bukkit.getWorld(worldToUnload);

            if (Bukkit.getServer().unloadWorld(world, save)) {
                Bukkit.getLogger().info("[MasterpikAPI] Unload world " + worldToUnload + " succefully");
            } else {
                Bukkit.getLogger().warning("[MasterpikAPI] Error when try to unload the world" + worldToUnload);
            }
        } else {
            Bukkit.getLogger().warning("[MasterpikAPI] World to unload do not exist");
        }*/
    }

    public static void ejectPlayers(String world, boolean tryLobby) {

        if (isWorldExist(world)) {

            ArrayList<Player> players = new ArrayList<>();
            players.addAll(Bukkit.getWorld(world).getPlayers());

            World tpWorld = null;

            if (tryLobby
                    && isWorldExist("lobby")) {
                tpWorld = Bukkit.getWorld("lobby");
            } else if (Bukkit.getWorlds() != null
                    && !Bukkit.getWorlds().isEmpty()
                    && Bukkit.getWorlds().size() > 0) {
                tpWorld = Bukkit.getWorlds().get(0);
            }

            if (tpWorld != null) {
                for (Player pl : players) {
                    pl.teleport(tpWorld.getSpawnLocation());
                }
            } else {
                for (Player pl : players) {
                    pl.chat("/hub");
                }
            }
        }
    }

    public static boolean isWorldExist(String world) {
        boolean check = false;

        int bucle = 0;

        while (bucle < Bukkit.getWorlds().size()) {

            if (Bukkit.getWorlds().get(bucle).getName().equals(world)) {
                check = true;
            }

            bucle++;
        }

        return check;
    }
    public static boolean isWorldExist(World world) {
        return isWorldExist(world.getName());
    }



    public static void applyWorldBorder(World world, WorldBorder worldBorder) {
        WorldBorder wwBorder = world.getWorldBorder();
        wwBorder.setCenter(worldBorder.getCenter());
        wwBorder.setDamageAmount(worldBorder.getDamageAmount());
        wwBorder.setDamageBuffer(worldBorder.getDamageBuffer());
        wwBorder.setSize(worldBorder.getSize());
        wwBorder.setWarningDistance(worldBorder.getWarningDistance());
        wwBorder.setWarningTime(worldBorder.getWarningTime());
    }

}
