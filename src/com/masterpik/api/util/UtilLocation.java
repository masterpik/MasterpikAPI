package com.masterpik.api.util;

import org.bukkit.Location;

public class UtilLocation {

    public static String locationToString(Location location) {
        return "{world : "+location.getWorld().getName()+" | x : "+location.getX()+" | y : "+location.getY()+" | z : "+location.getZ()+" | yaw : "+location.getYaw()+" | pitch : "+location.getPitch()+"}";
    }

}
