package com.masterpik.api.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class UtilJson {

    public static String getJsonFromObject(Object object) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        return gson.toJson(object);
    }
}
