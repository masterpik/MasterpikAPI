package com.masterpik.api.util.location;

import com.masterpik.api.util.UtilNumbers;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;

import java.util.ArrayList;

public enum Yaws {
    SOUTH(0.0f),
    SOUTH_WEST(45.0f),
    WEST(90.0f),
    NORTH_WEST(135.0f),
    NORTH(180.0f),
    NORTH_EAST(-135.0f),
    EAST(-90.0f),
    SOUTH_EAST(-45.0f),
    WEST_NORTH_WEST(112.5f),
    NORTH_NORTH_WEST(157.5f),
    NORTH_NORTH_EAST(-157.5f),
    EAST_NORTH_EAST(-112.5f),
    EAST_SOUTH_EAST(-67.5f),
    SOUTH_SOUTH_EAST(-22.5f),
    SOUTH_SOUTH_WEST(22.5f),
    WEST_SOUTH_WEST(67.5f);


    private float yaw;

    Yaws(float yaw) {
        this.yaw = yaw;
    }

    public float getYaw() {
        return yaw;
    }

    public BlockFace getBlockFace() {
        return BlockFace.valueOf(this.name());
    }

    @Override
    public String toString() {
        return "Yaws{" +
                "yaw=" + yaw +
                '}';
    }

    public static Yaws getNextYaw(float yaw) {
        float diff = 500.0f;

        Yaws next = null;

        for (Yaws check : Yaws.values()) {
            float tDiff = Math.abs(yaw-check.getYaw());
            if (tDiff < diff) {
                diff = tDiff;
                next = check;
            }
        }
        return next;
    }

    public static ArrayList<Yaws> facesToCheck(Yaws yaw) {
        ArrayList<Yaws> facesToCheck = new ArrayList<>();


        switch (yaw) {
            case NORTH: case EAST: case SOUTH: case WEST:
                facesToCheck.add(yaw);
                break;
            case NORTH_EAST: case NORTH_NORTH_EAST: case EAST_NORTH_EAST:
                facesToCheck.add(Yaws.NORTH);
                facesToCheck.add(Yaws.EAST);
                break;
            case NORTH_WEST: case WEST_NORTH_WEST: case NORTH_NORTH_WEST:
                facesToCheck.add(Yaws.NORTH);
                facesToCheck.add(Yaws.WEST);
                break;
            case SOUTH_EAST: case EAST_SOUTH_EAST: case SOUTH_SOUTH_EAST:
                facesToCheck.add(Yaws.SOUTH);
                facesToCheck.add(Yaws.EAST);
                break;
            case SOUTH_WEST: case SOUTH_SOUTH_WEST: case WEST_SOUTH_WEST:
                facesToCheck.add(Yaws.SOUTH);
                facesToCheck.add(Yaws.WEST);
                break;
        }

        return facesToCheck;

    }
}
