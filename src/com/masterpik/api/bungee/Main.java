package com.masterpik.api.bungee;

import com.masterpik.api.MasterpikAPI;
import com.masterpik.api.players.Players;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;


public class Main extends Plugin implements Listener{

    public static Plugin plugin;

    @Override
    public void onEnable() {
        plugin = this;

        ProxyServer.getInstance().getPluginManager().registerListener(this, this);

        MasterpikAPI.onEnable();

        MasterpikAPI.isSpigot = false;

    }

    @Override
    public void onDisable() {
        MasterpikAPI.onDisable();
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void ServerConnectedEvent(ServerConnectEvent event) {

        if (!event.isCancelled()) {

            if (event.getPlayer().getServer() == null) {
                Players.playerJoin(event.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerQuitEven(PlayerDisconnectEvent event) {
        if (Players.players.containsKey(event.getPlayer().getUniqueId())) {
            Players.players.remove(event.getPlayer().getUniqueId());
        }
    }

}
