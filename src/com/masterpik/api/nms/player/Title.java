package com.masterpik.api.nms.player;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.masterpik.api.json.ChatText;

public class Title {

    EnumWrappers.TitleAction action;

    ChatText text;

    int fadeIn;
    int stay;
    int fadeOut;


    public Title(EnumWrappers.TitleAction action, ChatText text, int fadeIn, int stay, int fadeOut) {
        this.action = action;
        this.text = text;
        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    public Title(EnumWrappers.TitleAction action, String text, int stay) {
        this.action = action;
        this.text = new ChatText("text");
        this.fadeIn = 0;
        this.stay = stay;
        this.fadeOut = 0;
    }

    public Title(EnumWrappers.TitleAction action) {
        this.action = action;
        this.text = new ChatText("");
        this.fadeIn = 0;
        this.stay = 0;
        this.fadeOut = 0;
    }


    public EnumWrappers.TitleAction getAction() {
        return action;
    }

    public Title setAction(EnumWrappers.TitleAction action) {
        this.action = action;
        return this;
    }

    public ChatText getText() {
        return text;
    }

    public Title setText(ChatText text) {
        this.text = text;
        return this;
    }

    public int getFadeIn() {
        return fadeIn;
    }

    public Title setFadeIn(int fadeIn) {
        this.fadeIn = fadeIn;
        return this;
    }

    public int getStay() {
        return stay;
    }

    public Title setStay(int stay) {
        this.stay = stay;
        return this;
    }

    public int getFadeOut() {
        return fadeOut;
    }

    public Title setFadeOut(int fadeOut) {
        this.fadeOut = fadeOut;
        return this;
    }
}
