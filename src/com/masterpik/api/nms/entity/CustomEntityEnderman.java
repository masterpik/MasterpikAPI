package com.masterpik.api.nms.entity;

import net.minecraft.server.v1_10_R1.EntityEnderman;
import net.minecraft.server.v1_10_R1.World;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftEnderman;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftLivingEntity;
import org.bukkit.entity.Enderman;
import org.bukkit.event.entity.CreatureSpawnEvent;

public class CustomEntityEnderman extends EntityEnderman {
    public CustomEntityEnderman(World world) {
        super(world);
    }

    @Override
    public void move(double d0, double d1, double d2){
    }


    public static Enderman spawn(Location location) {
        World mcWorld = (World) ((CraftWorld) location.getWorld()).getHandle();
        final CustomEntityEnderman customEntity = new CustomEntityEnderman(
                mcWorld);
        customEntity.setLocation(location.getX(), location.getY(),
                location.getZ(), location.getYaw(), location.getPitch());
        ((CraftLivingEntity) customEntity.getBukkitEntity())
                .setRemoveWhenFarAway(false);
        mcWorld.addEntity(customEntity, CreatureSpawnEvent.SpawnReason.CUSTOM);
        return (CraftEnderman) customEntity.getBukkitEntity();

    }
}
