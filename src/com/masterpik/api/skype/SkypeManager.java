package com.masterpik.api.skype;

import com.samczsun.skype4j.Skype;
import com.samczsun.skype4j.SkypeBuilder;
import com.samczsun.skype4j.Visibility;
import com.samczsun.skype4j.exceptions.ConnectionException;
import com.samczsun.skype4j.exceptions.InvalidCredentialsException;
import com.samczsun.skype4j.exceptions.NotParticipatingException;

public class SkypeManager {

    private static Skype skype;

    public static void init() {
        try {
            connect();
        } catch (ConnectionException | NotParticipatingException | InvalidCredentialsException e) {
            e.printStackTrace();
        }
    }

    public static void connect() throws ConnectionException, NotParticipatingException, InvalidCredentialsException {
        skype = new SkypeBuilder("masterpik_mc", "pacmasterpik14").withAllResources().build();

        System.out.println("Connexion au compte SKYPE");

        skype.login();

        skype.getEventDispatcher().registerListener(new SkypeEvents());

        skype.subscribe();

        System.out.println("Connexion à SKYPE réussie !");

        skype.setVisibility(Visibility.ONLINE);

    }

    public static void out() throws ConnectionException {
        skype.logout();
    }

    public static Skype getSkype() {
        return skype;
    }

    public static void setSkype(Skype skype) {
        SkypeManager.skype = skype;
    }
}
