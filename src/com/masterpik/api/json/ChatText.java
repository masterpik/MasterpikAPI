package com.masterpik.api.json;

import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilColor;

import java.util.List;

public class ChatText {

    //v-----Text input-----v
    String text;

    String translate;
    String[] with;

    Score score;

    String selector;
    //^-----Text input-----^

    //v-----Text styling-----v
    String color;

    boolean bold;

    boolean italic;

    boolean underlined;

    boolean strikethrough;

    boolean obfuscated;
    //^-----Text styling-----^

    //v-----Event listener-----v
    String insertion;

    ClickEvent clickEvent;

    HoverEvent hoverEvent;
    //^-----Event listener-----^
    //v-----Adding text-----v
    Object[] extra;
    //^-----Adding text-----^




    //v-----Text Constructor-----v
    public ChatText(String text, Colors color, boolean bold, boolean italic, boolean underlined, boolean strikethrough, boolean obfuscated, String insertion, ClickEvent clickEvent, HoverEvent hoverEvent) {
        this.text = text;
        this.translate = null;
        this.with = null;
        this.score = null;
        this.color = color.getColor();
        this.bold = bold;
        this.italic = italic;
        this.underlined = underlined;
        this.strikethrough = strikethrough;
        this.obfuscated = obfuscated;
        this.insertion = insertion;
        this.clickEvent = clickEvent;
        this.hoverEvent = hoverEvent;
    }
    //^-----Text Constructor-----^

    //v-----Simple text Constructor-----v
    public ChatText(String text, Colors color, boolean bold, boolean italic, boolean underlined, boolean strikethrough, boolean obfuscated) {
        this.text = text;
        this.translate = null;
        this.with = null;
        this.score = null;
        this.color = color.getColor();
        this.bold = bold;
        this.italic = italic;
        this.underlined = underlined;
        this.strikethrough = strikethrough;
        this.obfuscated = obfuscated;
        this.insertion = null;
        this.clickEvent = null;
        this.hoverEvent = null;
    }
    //^-----Simple text Constructor-----^

    //v-----Ultra simple text Constructor-----v
    public ChatText(String text, Colors color) {
        this.text = text;
        this.translate = null;
        this.with = null;
        this.score = null;
        this.color = color.getColor();
        this.bold = false;
        this.italic = false;
        this.underlined = false;
        this.strikethrough = false;
        this.obfuscated = false;
        this.insertion = null;
        this.clickEvent = null;
        this.hoverEvent = null;
    }
    //^-----Ultra simple text Constructor-----^


    //v-----Text builder-----v
    public ChatText(String text) {
        this.text = text;
    }
    //^-----Text builder-----^



    public HoverEvent getHoverEvent() {
        return hoverEvent;
    }

    public ChatText setHoverEvent(HoverEvent hoverEvent) {
        this.hoverEvent = hoverEvent;
        return this;
    }

    public String getText() {
        return text;
    }

    public ChatText setText(String text) {
        this.text = text;
        return this;
    }

    public String getTranslate() {
        return translate;
    }

    public ChatText setTranslate(String translate) {
        this.translate = translate;
        return this;
    }

    public String[] getWith() {
        return with;
    }

    public ChatText setWith(String[] with) {
        this.with = with;
        return this;
    }

    public Score getScore() {
        return score;
    }

    public ChatText setScore(Score score) {
        this.score = score;
        return this;
    }

    public String getSelector() {
        return selector;
    }

    public ChatText setSelector(String selector) {
        this.selector = selector;
        return this;
    }

    public String getColor() {
        return color;
    }

    public ChatText setColor(Colors color) {
        this.color = color.getColor();
        return this;
    }

    public boolean isBold() {
        return bold;
    }

    public ChatText setBold(boolean bold) {
        this.bold = bold;
        return this;
    }

    public boolean isItalic() {
        return italic;
    }

    public ChatText setItalic(boolean italic) {
        this.italic = italic;
        return this;
    }

    public boolean isUnderlined() {
        return underlined;
    }

    public ChatText setUnderlined(boolean underlined) {
        this.underlined = underlined;
        return this;
    }

    public boolean isStrikethrough() {
        return strikethrough;
    }

    public ChatText setStrikethrough(boolean strikethrough) {
        this.strikethrough = strikethrough;
        return this;
    }

    public boolean isObfuscated() {
        return obfuscated;
    }

    public ChatText setObfuscated(boolean obfuscated) {
        this.obfuscated = obfuscated;
        return this;
    }

    public String getInsertion() {
        return insertion;
    }

    public ChatText setInsertion(String insertion) {
        this.insertion = insertion;
        return this;
    }

    public ClickEvent getClickEvent() {
        return clickEvent;
    }

    public ChatText setClickEvent(ClickEvent clickEvent) {
        this.clickEvent = clickEvent;
        return this;
    }

    public Object[] getExtra() {
        return extra;
    }

    public ChatText setExtra(Object[] extra) {
        this.extra = extra;
        return this;
    }

    public ChatText(ChatText text) {
        this.text = text.getText();
        this.translate = text.getTranslate();
        this.with = text.getWith();
        this.score = text.getScore();
        this.selector = text.getSelector();
        this.color = text.getColor();
        this.bold = text.isBold();
        this.italic = text.isItalic();
        this.underlined = text.isUnderlined();
        this.strikethrough = text.isStrikethrough();
        this.obfuscated = text.isObfuscated();
        this.insertion = text.getInsertion();
        this.clickEvent = text.getClickEvent();
        this.hoverEvent = text.getHoverEvent();
        this.extra = text.getExtra();
    }

    public String toChatString() {
        String re = "";
        if (Colors.valueOf(this.color.toUpperCase()) != null) {
            re = re + "§" + Colors.valueOf(this.color.toUpperCase()).getCode();
        }

        if (this.bold) re = re+"§l";
        if (this.italic) re = re+"§o";
        if (this.underlined) re = re+"§n";
        if (this.strikethrough) re = re+"§m";
        if (this.obfuscated) re = re+"§k";

        re = re+this.text;

        for (Object t : this.extra) {
            if (t instanceof ChatText) {
                re = re+((ChatText) t).toChatString();
            }
        }

        return re;
    }
}
