package com.masterpik.api.json;

public class HoverEvent {

    String action;

    Object[] value;


    public HoverEvent(HoverEvents action, ChatText value) {
        this.action = action.getAction();
        this.value = new ChatText[]{value};
    }

    public HoverEvent(HoverEvents action, ChatText[] value) {
        this.action = action.getAction();
        this.value = value;
    }

    public HoverEvent(HoverEvents action, Object[] value) {
        this.action = action.getAction();
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(HoverEvents action) {
        this.action = action.getAction();
    }

    public Object[] getValue() {
        return value;
    }

    public void setValue(ChatText value) {
        this.value = new ChatText[]{value};
    }

    public void setValue(ChatText[] value) {
        this.value = value;
    }

    public void setValue(Object[] value) {
        this.value = value;
    }
}

