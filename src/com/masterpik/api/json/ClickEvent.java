package com.masterpik.api.json;

public class ClickEvent {

    String action;

    String value;


    public ClickEvent(ClickEvents action, String value) {
        this.action = action.getAction();
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(ClickEvents action) {
        this.action = action.getAction();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
