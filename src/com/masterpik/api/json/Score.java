package com.masterpik.api.json;

public class Score {

    String name;
    String objective;
    String value;


    public Score(String name, String objective) {
        this.name = name;
        this.objective = objective;
    }

    public Score(String name, String objective, String value) {
        this.name = name;
        this.objective = objective;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
