package com.masterpik.api.json;

public enum ClickEvents {
    OPEN_URL("open_url"),
    OPEN_FILE("open_file"),
    RUN_COMMAND("run_command"),
    SUGGEST_COMMAND("suggest_command"),
    CHANGE_PAGE("change_page");

    private String action;

    ClickEvents(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
}
