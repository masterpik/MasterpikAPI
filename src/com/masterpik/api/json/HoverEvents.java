package com.masterpik.api.json;

public enum HoverEvents {
    SHOW_TEXT("show_text"),
    SHOW_ACHIEVEMENT("show_achievement"),
    SHOW_ITEM("show_item"),
    SHOW_ENTITY("show_entity");


    private String action;

    HoverEvents(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
}
