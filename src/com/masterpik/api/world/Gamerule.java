package com.masterpik.api.world;

import com.avaje.ebean.config.ServerConfig;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Objects;

public class Gamerule {
    private boolean commandBlockOutput;
    private boolean disableElytraMovementCheck;
    private boolean doDaylightCycle;
    private boolean doEntityDrops;
    private boolean doFireTick;
    private boolean doMobLoot;
    private boolean doMobSpawning;
    private boolean doTileDrops;
    private boolean keepInventory;
    private boolean logAdminCommands;
    private boolean mobGriefing;
    private boolean naturalRegeneration;
    private int randomTickSpeed;
    private boolean reducedDebugInfo;
    private boolean sendCommandFeedback;
    private boolean showDeathMessages;
    private int spawnRadius;
    private boolean spectatorsGenerateChunks;

    private boolean worldAutoSave;
    private int worldWaterAnimalSpawnLimit;
    private int worldAmbientSpawnLimit;
    private int worldAnimalSpawnLimit;
    private Difficulty worldDifficulty;
    private long worldFullTime;
    private boolean worldKeepSpawnInMemory;
    private int worldMonsterSpawnLimit;
    private boolean worldPVP;
    private int worldTicksPerAnimalSpawns;
    private int worldTicksPerMonsterSpawns;


    private HashMap<String, Object> customs;


    public Gamerule(boolean commandBlockOutput, boolean disableElytraMovementCheck, boolean doDaylightCycle, boolean doEntityDrops, boolean doFireTick, boolean doMobLoot, boolean doMobSpawning, boolean doTileDrops, boolean keepInventory, boolean logAdminCommands, boolean mobGriefing, boolean naturalRegeneration, int randomTickSpeed, boolean reducedDebugInfo, boolean sendCommandFeedback, boolean showDeathMessages, int spawnRadius, boolean spectatorsGenerateChunks, boolean worldAutoSave, int worldWaterAnimalSpawnLimit, int worldAmbientSpawnLimit, int worldAnimalSpawnLimit, Difficulty worldDifficulty, long worldFullTime, boolean worldKeepSpawnInMemory, int worldMonsterSpawnLimit, boolean worldPVP, int worldTicksPerAnimalSpawns, int worldTicksPerMonsterSpawns, HashMap<String, Object> customs) {
        this.commandBlockOutput = commandBlockOutput;
        this.disableElytraMovementCheck = disableElytraMovementCheck;
        this.doDaylightCycle = doDaylightCycle;
        this.doEntityDrops = doEntityDrops;
        this.doFireTick = doFireTick;
        this.doMobLoot = doMobLoot;
        this.doMobSpawning = doMobSpawning;
        this.doTileDrops = doTileDrops;
        this.keepInventory = keepInventory;
        this.logAdminCommands = logAdminCommands;
        this.mobGriefing = mobGriefing;
        this.naturalRegeneration = naturalRegeneration;
        this.randomTickSpeed = randomTickSpeed;
        this.reducedDebugInfo = reducedDebugInfo;
        this.sendCommandFeedback = sendCommandFeedback;
        this.showDeathMessages = showDeathMessages;
        this.spawnRadius = spawnRadius;
        this.spectatorsGenerateChunks = spectatorsGenerateChunks;
        this.worldAutoSave = worldAutoSave;
        this.worldWaterAnimalSpawnLimit = worldWaterAnimalSpawnLimit;
        this.worldAmbientSpawnLimit = worldAmbientSpawnLimit;
        this.worldAnimalSpawnLimit = worldAnimalSpawnLimit;
        this.worldDifficulty = worldDifficulty;
        this.worldFullTime = worldFullTime;
        this.worldKeepSpawnInMemory = worldKeepSpawnInMemory;
        this.worldMonsterSpawnLimit = worldMonsterSpawnLimit;
        this.worldPVP = worldPVP;
        this.worldTicksPerAnimalSpawns = worldTicksPerAnimalSpawns;
        this.worldTicksPerMonsterSpawns = worldTicksPerMonsterSpawns;
        this.customs = customs;
    }

    public Gamerule(boolean commandBlockOutput, boolean disableElytraMovementCheck, boolean doDaylightCycle, boolean doEntityDrops, boolean doFireTick, boolean doMobLoot, boolean doMobSpawning, boolean doTileDrops, boolean keepInventory, boolean logAdminCommands, boolean mobGriefing, boolean naturalRegeneration, int randomTickSpeed, boolean reducedDebugInfo, boolean sendCommandFeedback, boolean showDeathMessages, int spawnRadius, boolean spectatorsGenerateChunks, boolean worldAutoSave, int worldWaterAnimalSpawnLimit, int worldAmbientSpawnLimit, int worldAnimalSpawnLimit, Difficulty worldDifficulty, long worldFullTime, boolean worldKeepSpawnInMemory, int worldMonsterSpawnLimit, boolean worldPVP, int worldTicksPerAnimalSpawns, int worldTicksPerMonsterSpawns) {
        this(commandBlockOutput, disableElytraMovementCheck, doDaylightCycle, doEntityDrops, doFireTick, doMobLoot, doMobSpawning, doTileDrops, keepInventory, logAdminCommands, mobGriefing, naturalRegeneration, randomTickSpeed, reducedDebugInfo, sendCommandFeedback, showDeathMessages, spawnRadius, spectatorsGenerateChunks, worldAutoSave, worldWaterAnimalSpawnLimit, worldAmbientSpawnLimit, worldAnimalSpawnLimit, worldDifficulty, worldFullTime, worldKeepSpawnInMemory, worldMonsterSpawnLimit, worldPVP, worldTicksPerAnimalSpawns, worldTicksPerMonsterSpawns,  new HashMap<>());
    }

    public Gamerule() {
        this(true, false, true, true, true, true, true, true, false, true, true, true, 3, false, true, true, 10, true, true, Bukkit.getWaterAnimalSpawnLimit(), Bukkit.getAmbientSpawnLimit(), Bukkit.getAnimalSpawnLimit(), Difficulty.NORMAL, 0L, true, Bukkit.getMonsterSpawnLimit(), true, Bukkit.getTicksPerAnimalSpawns(), Bukkit.getTicksPerMonsterSpawns());
    }

    public boolean isCommandBlockOutput() {
        return commandBlockOutput;
    }

    public Gamerule setCommandBlockOutput(boolean commandBlockOutput) {
        this.commandBlockOutput = commandBlockOutput;
        return this;
    }

    public boolean isDisableElytraMovementCheck() {
        return disableElytraMovementCheck;
    }

    public Gamerule setDisableElytraMovementCheck(boolean disableElytraMovementCheck) {
        this.disableElytraMovementCheck = disableElytraMovementCheck;
        return this;
    }

    public boolean isDoDaylightCycle() {
        return doDaylightCycle;
    }

    public Gamerule setDoDaylightCycle(boolean doDaylightCycle) {
        this.doDaylightCycle = doDaylightCycle;
        return this;
    }

    public boolean isDoEntityDrops() {
        return doEntityDrops;
    }

    public Gamerule setDoEntityDrops(boolean doEntityDrops) {
        this.doEntityDrops = doEntityDrops;
        return this;
    }

    public boolean isDoFireTick() {
        return doFireTick;
    }

    public Gamerule setDoFireTick(boolean doFireTick) {
        this.doFireTick = doFireTick;
        return this;
    }

    public boolean isDoMobLoot() {
        return doMobLoot;
    }

    public Gamerule setDoMobLoot(boolean doMobLoot) {
        this.doMobLoot = doMobLoot;
        return this;
    }

    public boolean isDoMobSpawning() {
        return doMobSpawning;
    }

    public Gamerule setDoMobSpawning(boolean doMobSpawning) {
        this.doMobSpawning = doMobSpawning;
        return this;
    }

    public boolean isDoTileDrops() {
        return doTileDrops;
    }

    public Gamerule setDoTileDrops(boolean doTileDrops) {
        this.doTileDrops = doTileDrops;
        return this;
    }

    public boolean isKeepInventory() {
        return keepInventory;
    }

    public Gamerule setKeepInventory(boolean keepInventory) {
        this.keepInventory = keepInventory;
        return this;
    }

    public boolean isLogAdminCommands() {
        return logAdminCommands;
    }

    public Gamerule setLogAdminCommands(boolean logAdminCommands) {
        this.logAdminCommands = logAdminCommands;
        return this;
    }

    public boolean isMobGriefing() {
        return mobGriefing;
    }

    public Gamerule setMobGriefing(boolean mobGriefing) {
        this.mobGriefing = mobGriefing;
        return this;
    }

    public boolean isNaturalRegeneration() {
        return naturalRegeneration;
    }

    public Gamerule setNaturalRegeneration(boolean naturalRegeneration) {
        this.naturalRegeneration = naturalRegeneration;
        return this;
    }

    public int getRandomTickSpeed() {
        return randomTickSpeed;
    }

    public Gamerule setRandomTickSpeed(int randomTickSpeed) {
        this.randomTickSpeed = randomTickSpeed;
        return this;
    }

    public boolean isReducedDebugInfo() {
        return reducedDebugInfo;
    }

    public Gamerule setReducedDebugInfo(boolean reducedDebugInfo) {
        this.reducedDebugInfo = reducedDebugInfo;
        return this;
    }

    public boolean isSendCommandFeedback() {
        return sendCommandFeedback;
    }

    public Gamerule setSendCommandFeedback(boolean sendCommandFeedback) {
        this.sendCommandFeedback = sendCommandFeedback;
        return this;
    }

    public boolean isShowDeathMessages() {
        return showDeathMessages;
    }

    public Gamerule setShowDeathMessages(boolean showDeathMessages) {
        this.showDeathMessages = showDeathMessages;
        return this;
    }

    public int getSpawnRadius() {
        return spawnRadius;
    }

    public Gamerule setSpawnRadius(int spawnRadius) {
        this.spawnRadius = spawnRadius;
        return this;
    }

    public boolean isSpectatorsGenerateChunks() {
        return spectatorsGenerateChunks;
    }

    public Gamerule setSpectatorsGenerateChunks(boolean spectatorsGenerateChunks) {
        this.spectatorsGenerateChunks = spectatorsGenerateChunks;
        return this;
    }

    public HashMap<String, Object> getCustoms() {
        return customs;
    }

    public void setCustoms(HashMap<String, Object> customs) {
        this.customs = customs;
    }


    public void applyGamerule(World world) {
        for (Field field : this.getClass().getDeclaredFields()) {
            if (!field.getType().equals(HashMap.class)) {
                field.setAccessible(true);
                if (!field.getName().startsWith("world")) {
                    try {
                        world.setGameRuleValue(field.getName(), field.get(this).toString());
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        for (String str : this.customs.keySet()) {
            world.setGameRuleValue(str, this.customs.get(str).toString());
        }

        world.setAutoSave(this.worldAutoSave);
        world.setWaterAnimalSpawnLimit(this.worldWaterAnimalSpawnLimit);
        world.setAmbientSpawnLimit(this.worldAmbientSpawnLimit);
        world.setAnimalSpawnLimit(this.worldAnimalSpawnLimit);
        world.setDifficulty(this.worldDifficulty);
        world.setFullTime(this.worldFullTime);
        world.setKeepSpawnInMemory(this.worldKeepSpawnInMemory);
        world.setMonsterSpawnLimit(this.worldMonsterSpawnLimit);
        world.setPVP(this.worldPVP);
        world.setTicksPerAnimalSpawns(this.worldTicksPerAnimalSpawns);
        world.setTicksPerMonsterSpawns(this.worldTicksPerMonsterSpawns);
    }
}
