package com.masterpik.api;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.masterpik.api.commands.*;
import com.masterpik.api.events.Events;
import com.masterpik.api.nms.entity.CustomEntityType;
import com.masterpik.api.players.Players;
import com.masterpik.api.world.Gamerule;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;


public class Main extends JavaPlugin implements Listener{

    public static Plugin plugin;

    public static HashMap<UUID, PermissionAttachment> perms;

    @Override
    public void onEnable() {
        plugin = this;

        getCommand("wtp").setExecutor(new WorldCommands());
        getCommand("wload").setExecutor(new WorldCommands());
        getCommand("wunload").setExecutor(new WorldCommands());
        getCommand("wlist").setExecutor(new WorldCommands());
        getCommand("soundplay").setExecutor(new SoundsCo());

        getCommand("lag").setExecutor(new Lag());

        Crash.init();
        this.getCommand("crash").setExecutor(new Crash());

        BukkitPerms.init();
        perms = new HashMap<>();
        this.getCommand("bperm").setExecutor(new BukkitPerms());

        CustomEntityType.registerEntities();

        /*ArrayList<String> helpAliases = new ArrayList<>();

        if (this.getCommand("help").getAliases() != null) {
            helpAliases.addAll(this.getCommand("help").getAliases());
        }

        helpAliases.add("bukkithelp");
        helpAliases.add("spigothelp");
        helpAliases.add("bhelp");
        helpAliases.add("shelp");

        this.getCommand("help").setAliases(helpAliases);*/
        MasterpikAPI.isSpigot = true;
        MasterpikAPI.onEnable();
        Events.init();

        Bukkit.getPluginManager().registerEvents(this, this);
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

    }

    @Override
    public void onDisable() {
        CustomEntityType.unregisterEntities();
        MasterpikAPI.onDisable();

        if (perms != null
                && !perms.isEmpty()
                && perms.size() > 0) {
            for (Player pl : Bukkit.getOnlinePlayers()) {
                if (perms.containsKey(pl.getUniqueId())) {
                    pl.removeAttachment(perms.get(pl.getUniqueId()));
                    perms.remove(pl.getUniqueId());
                }
            }
        }
    }


    @EventHandler(priority = EventPriority.HIGH)
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        Players.playerJoin(event.getPlayer().getUniqueId());
        perms.put(event.getPlayer().getUniqueId(), event.getPlayer().addAttachment(this));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerQuitEven(PlayerQuitEvent event) {
        if (Players.players.containsKey(event.getPlayer().getUniqueId())) {
            Players.players.remove(event.getPlayer().getUniqueId());
        }

        if (Crash.crash.contains(event.getPlayer())) {
            Crash.crash.remove(event.getPlayer());
        }

        if (perms != null
                && !perms.isEmpty()
                && perms.size() > 0
                && perms.containsKey(event.getPlayer().getUniqueId())) {
            event.getPlayer().removeAttachment(perms.get(event.getPlayer().getUniqueId()));
            perms.remove(event.getPlayer().getUniqueId());
        }
    }

}
