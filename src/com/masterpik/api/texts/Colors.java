package com.masterpik.api.texts;

public enum Colors {
    WHITE("white", 'f', "Blanc", 0, 15),
    YELLOW("yellow", 'e', "Jaune", 4, 11),
    LIGHT_PURPLE("light_purple", 'd', "Violet", 2, 13),
    RED("red", 'c', "Rouge", 14, 1),
    AQUA("aqua", 'b', "Bleu", 3, 12),
    GREEN("green", 'a', "Vert", 5, 10),
    BLUE("blue", '9', "Bleu", 11, 4),
    DARK_GRAY("dark_gray", '8', "Gris", 7, 8),
    GRAY("gray", '7', "Gris", 8, 7),
    GOLD("gold", '6', "Orange", 1, 14),
    DARK_PURPLE("dark_purple", '5', "Violet", 10, 5),
    DARK_RED("dark_red", '4', "Rouge", 14, 1),
    DARK_AQUA("dark_aqua", '3', "Bleu", 9, 6),
    DARK_GREEN("dark_green", '2', "Vert", 13, 2),
    DARK_BLUE("dark_blue", '1', "Bleu", 11, 4),
    BLACK("black", '0', "Noir", 15, 0),
    PINK("Rose", 6, 9),
    BROWN("Marron", 12, 3);

    private String color;
    private char code;

    private String frName;

    private byte blockCode;
    private byte bannerCode;

    Colors(String color, char code, String frName) {
        this.color = color;
        this.code = code;
        this.frName = frName;
    }

    Colors(String frName, byte blockCode, byte bannerCode) {
        this.frName = frName;
        this.blockCode = blockCode;
        this.bannerCode = bannerCode;
    }

    Colors(String frName, int blockCode, int bannerCode) {
        this.frName = frName;

        try {
            this.blockCode = Byte.parseByte(Integer.toString(blockCode));
        } catch (NumberFormatException e) {
            this.blockCode = (byte) 0;
        }

        try {
            this.bannerCode = Byte.parseByte(Integer.toString(bannerCode));
        } catch (NumberFormatException e) {
            this.bannerCode = (byte) 0;
        }
    }

    Colors(String color, char code, String frName, byte blockCode, byte bannerCode) {
        this.color = color;
        this.code = code;
        this.frName = frName;
        this.blockCode = blockCode;
        this.bannerCode = bannerCode;
    }


    Colors(String color, char code, String frName, int blockCode, int bannerCode) {
        this.color = color;
        this.code = code;
        this.frName = frName;

        try {
            this.blockCode = Byte.parseByte(Integer.toString(blockCode));
        } catch (NumberFormatException e) {
            this.blockCode = (byte) 0;
        }

        try {
            this.bannerCode = Byte.parseByte(Integer.toString(bannerCode));
        } catch (NumberFormatException e) {
            this.bannerCode = (byte) 0;
        }
    }



    public String getColor() {
        return color;
    }

    public char getCode() {
        return code;
    }

    public String getFrName() {
        return frName;
    }

    public byte getBlockCode() {
        return blockCode;
    }

    public byte getBannerCode() {
        return bannerCode;
    }
}