package com.masterpik.api;

import com.masterpik.api.players.Players;
import com.masterpik.api.players.status.StatusManagement;
import com.masterpik.api.skype.SkypeManager;
import com.masterpik.api.teamspeak.Teamspeak;
import com.masterpik.database.db.Connect;
import com.samczsun.skype4j.exceptions.ConnectionException;

import java.sql.Connection;

public class MasterpikAPI {

    public static boolean isSpigot;
    public static Connection connection;

    public static void onEnable() {

        connection = Connect.getConnection();

        StatusManagement.StatusInit();
        Players.playersInit();
        Teamspeak.init();
        SkypeManager.init();

    }

    public static void onDisable() {
        Teamspeak.exit();
        try {
            SkypeManager.out();
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }


}
